# Friss Fraud Api


> Fraud detection Api, with 2 endpoints

1 - Receives and stores a person

2 - Calculates the probability that 2 people are the same physical person.



## 💻 Tech stack

The project was developed using .NET 5 and ASPNET Core for its Api.
For persistance, it uses MongoDb, with an implementation that uses the regular C# driver internally.
Logging was done through Serilog, using its MongoDb provider to save data.
There is a configurable CSV file for dimninutives/nicknames like Andred/Andy, Edward/Ed and so on. It's currently in Files/names.csv in the Api project.
For test projects, the solution uses XUnit, NSubstitute and FluentAssertions.


## 🚀 Installing 

To install the project, one should copy this repo and build the source code. FRISS.Fraud.Api is the main project with the ASPNET host.
For persistence, it needs a MongoDb instance, that can be installed and configured on appsettings. There is a docker-compose.yml file that puts a local MongoDb container online.

The solution is OS agnostic with MongoDb and .NET 5, but all tests were done on a Windows 10 machine with Linux containers.

## ☕ Using the project
There are 4 endpoints in the api, which sits at "/", "create" and "match" for the basic functionalities, and also "status" for health checking, and "get" to get a person by its id.

Create
Create is a post method that uses the following payload:
{
  "firstName": "string",
  "lastNAme": "string",
  "identificationNumber": 12345,
  "dateOfBirth": "2001-07-07"
}

Match
Match is a post method that uses the following payload:
{
  "firstPersonId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "secondPersonId": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
}

The host api provides access to Swagger, following the basic ASPNET convention "/swagger/index.html".

### Future improvements

The project was coded in 2 days, so there are lots of possible improvements to be done:

- Security -> Everything is public at the moment. The Api should have authentication and authorization through a different login endpoint that should provide a JWT token for access, this token should be used to validate access for its endpoints.
Configuration files have plain database connection strings in there, and it does not use a user/password. Ideally, the database should be password protected, and those credentials should be stored in environment variables, like pipeline secrets.
- Request caching -> There is no caching implementation at the moment. Since matching is done through 2 person ids, a key value storage with those 2 ids as a key and the response payload as value should improve matching performance and avoid database calls.
- Configurable matching rules -> Although there are generic IPersonMatcher implementations, their match percentages are all hardcoded. A first step could be moving those percentages to constructors, so that they could be changed and configured. PersonMatcher is a class that groups all the individual matching classes (NameMatcher, IdentificationNumberMatcher, DateOfBirthMatcher), but instead of doing it like this, it could be changed into a GroupMatcher class that received a list of IPersonMatcher classes and used all of them to match 2 people. Also, those matchers could become entities and be persisted, thus giving more flexibility to matching. With those changes, MatchPeople should not be part of PersonService and be moved to a MatchService of its own.
- Containers -> Although there are different docker compose files for MongoDb, the application does not have a fully functional docker compose. There is a dockerfile, but there is a problem with dotnet core certificates for HTTPS. Solving that problem would enable a docker compose for the application that could run the api and the database together.
- CI/CD -> Everything was done locally, so there are no pipelines at the moment. Ideally, there should be a pipeline that built the project, ran all tests and deployed the code. Also, a pull request pipeline with build, tests and metrics like Sonar could improve the repository.
- Api endpoints -> Only 2 endpoints were created, one for creating a person and another one for matching 2 people. CRUD capabilities for person and maybe a list method could improve usability of the Api. The way it is now, there are only person Guids returned by the create endpoint.
- Similar names -> Name matching uses StringiSimilarity for typos on first names, and there could be lots of false positives with the current implementation. Maybe changing it to a Jaro-Winkler or Levenshtein library could improve similarity string detection.
