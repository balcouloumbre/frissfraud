﻿using FluentAssertions;
using FRISS.Fraud.Domain.Interfaces;
using System;
using Xunit;

namespace FRISS.Fraud.Domain.Tests
{
    public class PersonTests
    {
        private IPerson testSubject;
        private readonly string firstName;
        private readonly string lastName;
        private readonly int identificationNumber;
        private readonly DateTime dateOfBirth;

        public PersonTests()
        {
            firstName = "Andrew";
            lastName = "Craw";
            identificationNumber = 123456;
            dateOfBirth = DateTime.Now.AddYears(-30);

            testSubject = new Person(firstName, lastName);
        }

        [Fact]
        public void ShouldBeCreatedWithAnId()
        {
            testSubject.Id.Should().NotBeEmpty();
        }

        [Fact]
        public void ShouldBeCreatedWithAFirstNameAndALastName()
        {
            testSubject.Should().NotBeNull();

            testSubject.FirstName.Should().Be(firstName);
            testSubject.LastName.Should().Be(lastName);
        }

        [Fact]
        public void ShouldNotBeCreatedWithANullFirstName()
        {
            testSubject = null;

            Action creation = () =>
            {
                testSubject = new Person(null, lastName);
            };

            creation.Should().Throw<ArgumentNullException>();
            testSubject.Should().BeNull();
        }

        [Theory]
        [InlineData("")]
        [InlineData("   ")]
        public void ShouldNotBeCreatedWithAnInvalidFirstName(string invalidFirstName)
        {
            testSubject = null;

            Action creation = () =>
            {
                testSubject = new Person(invalidFirstName, lastName);
            };

            creation.Should().Throw<ArgumentException>();
            testSubject.Should().BeNull();
        }

        [Fact]
        public void ShouldNotBeCreatedWithANullLastName()
        {
            testSubject = null;

            Action creation = () =>
            {
                testSubject = new Person(firstName, null);
            };

            creation.Should().Throw<ArgumentNullException>();
            testSubject.Should().BeNull();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        public void ShouldNotBeCreatedWithAnInvalidLasttName(string invalidLasttName)
        {
            testSubject = null;

            Action creation = () =>
            {
                testSubject = new Person(firstName, invalidLasttName);
            };

            creation.Should().Throw<ArgumentException>();
            testSubject.Should().BeNull();
        }

        [Fact]
        public void ShouldBeAbleToAddAnIdentificationNumber()
        {
            testSubject.IdentificationNumber.Should().NotHaveValue();

            testSubject.SetIdentificationNumber(identificationNumber);
            testSubject.IdentificationNumber.Should().HaveValue();
            testSubject.IdentificationNumber.Value.Should().Be(identificationNumber);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldNotBeAbleToSetANegativeOrZeroIdentificationNumber(int identificationNumber)
        {
            Action setIdentificationNumber = () =>
            {
                testSubject.SetIdentificationNumber(identificationNumber);
            };

            setIdentificationNumber.Should().Throw<ArgumentException>();
        }

        [Fact]
        public void ShouldBeAbleToSetADateOfBirth()
        {
            testSubject.DateOfBirth.Should().NotHaveValue();

            testSubject.SetDateOfBirth(dateOfBirth);
            testSubject.DateOfBirth.Should().HaveValue();
            testSubject.DateOfBirth.Value.Should().Be(dateOfBirth);
        }

        [Fact]
        public void ShouldNotBeAbleToSetAFutureDateOfBirth()
        {
            Action setDateOfBirth = () =>
            {
                testSubject.SetDateOfBirth(DateTime.Now.AddDays(1));
            };

            setDateOfBirth.Should().Throw<ArgumentException>();
        }
    }
}
