﻿using FluentAssertions;
using FRISS.Fraud.Domain.Interfaces;
using FRISS.Fraud.Domain.Matching;
using NSubstitute;
using System;
using System.Collections.Generic;
using Xunit;

namespace FRISS.Fraud.Domain.Tests.Matching
{
    public class NameMatcherTests
    {
        private IPersonMatcher testSubject;
        private IPerson aPerson;
        private IPerson anotherPerson;
        private string aFirstName;
        private string anotherFirstName;
        private string aLastName;
        private string anotherLastName;

        public NameMatcherTests()
        {
            aFirstName = "Andrew";
            aLastName = "Craw";
            anotherFirstName = "Petty";
            anotherLastName = "Smith";

            aPerson = Substitute.For<IPerson>();
            aPerson.FirstName.Returns(aFirstName);
            aPerson.LastName.Returns(aLastName);

            anotherPerson = Substitute.For<IPerson>();
            anotherPerson.FirstName.Returns(anotherFirstName);
            anotherPerson.LastName.Returns(anotherLastName);

            testSubject = new NameMatcher();
        }

        [Fact]
        public void ShouldReturnZeroMatchForDifferentFirstAndLastNames()
        {
            testSubject.Match(aPerson, anotherPerson).Should().Be(0.0D);
        }

        [Fact]
        public void ShouldReturn40PercentMatchForTheSameLastName()
        {
            anotherPerson.LastName.Returns(aLastName);

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.4D);
        }

        [Fact]
        public void ShouldReturn20PercentMatchForTheSameFirstName()
        {
            anotherPerson.FirstName.Returns(aFirstName);

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.2D);
        }

        [Fact]
        public void ShouldReturn60PercentMatchForTheSameFirstAndLastNameS()
        {
            anotherPerson.FirstName.Returns(aFirstName);
            anotherPerson.LastName.Returns(aLastName);

            testSubject.Match(aPerson, anotherPerson).Should().BeApproximately(0.6D, 0.00001D);
        }

        [Fact]
        public void ShouldNotBeAbleToMatchNullPeople()
        {
            Action match = () =>
            {
                testSubject.Match(aPerson, anotherPerson);
            };

            aPerson = null;
            match.Should().Throw<ArgumentNullException>();

            aPerson = Substitute.For<IPerson>();
            anotherPerson = null;

            match.Should().Throw<ArgumentNullException>();

            aPerson = null;
            match.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void ShouldMatchNamesWithSpecialCharactersAndUpperCase()
        {
            aFirstName = "Mary Ann";
            anotherFirstName = " Màry  ANN ";
            aLastName = "DANGELO";
            anotherLastName = "D'angelo";

            aPerson.FirstName.Returns(aFirstName);
            aPerson.LastName.Returns(aLastName);            
            anotherPerson.FirstName.Returns(anotherFirstName);
            anotherPerson.LastName.Returns(anotherLastName);

            testSubject.Match(aPerson, anotherPerson).Should().BeApproximately(0.6D, 0.00001D);
        }

        [Fact]
        public void ShouldMatchFirstNamesAndInitials()
        {
            aFirstName = "John";
            anotherFirstName = "J.";

            aPerson.FirstName.Returns(aFirstName);            
            anotherPerson.FirstName.Returns(anotherFirstName);

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.15D);

            aPerson.FirstName.Returns(anotherFirstName);
            anotherPerson.FirstName.Returns(aFirstName);

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.15D);
        }

        [Fact]
        public void ShouldMatchFirstNamesAndInitialsForCompositeNames()
        {
            aFirstName = "Mary Ann";
            anotherFirstName = "M. A.";

            aPerson.FirstName.Returns(aFirstName);
            anotherPerson.FirstName.Returns(anotherFirstName);

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.15D);

            aPerson.FirstName.Returns(anotherFirstName);
            anotherPerson.FirstName.Returns(aFirstName);

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.15D);
        }

        [Fact]
        public void ShouldBeAbleToAddFirstNameSimilaritiesAndDetectThem()
        {
            aFirstName = "Andy";
            anotherFirstName = "Andrew";

            aPerson.FirstName.Returns(aFirstName);
            anotherPerson.FirstName.Returns(anotherFirstName);

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.0D);

            List<string> similarities = new List<string>()
            {
                "Andy",
                "Drew"
            };

            NameMatcher.AddNameSimilarity("Andrew", similarities);

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.15D);
        }

        [Fact]
        public void ShouldMatchFirstNamesWithSimilarStrings()
        {
            aFirstName = "Andew";
            anotherFirstName = "Andrew";

            aPerson.FirstName.Returns(aFirstName);
            anotherPerson.FirstName.Returns(anotherFirstName);

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.15D);            
        }
    }
}
