﻿using FluentAssertions;
using FRISS.Fraud.Domain.Interfaces;
using FRISS.Fraud.Domain.Matching;
using NSubstitute;
using System;
using Xunit;

namespace FRISS.Fraud.Domain.Tests.Matching
{
    public class PersonMatcherTests
    {
        private readonly IPersonMatcher testSubject;

        private IPerson aPerson;
        private IPerson anotherPerson;
        private string aFirstName;
        private string anotherFirstName;
        private string aLastName;
        private string anotherLastName;
        private int? anIdentificationNumber;
        private int? anotherIdentificationNumber;
        private DateTime? aDateOfBirth;
        private DateTime? anotherDateOfBirth;

        public PersonMatcherTests()
        {
            aFirstName = "Andrew";
            aLastName = "Craw";
            anotherFirstName = "Petty";
            anotherLastName = "Smith";
            aDateOfBirth = DateTime.Now.AddYears(-30);
            anotherDateOfBirth = DateTime.Now.AddYears(-25);
            anIdentificationNumber = 123456;
            anotherIdentificationNumber = 234567;

            aPerson = Substitute.For<IPerson>();
            aPerson.FirstName.Returns(aFirstName);
            aPerson.LastName.Returns(aLastName);
            aPerson.DateOfBirth.Returns(aDateOfBirth);
            aPerson.IdentificationNumber.Returns(anIdentificationNumber);

            anotherPerson = Substitute.For<IPerson>();
            anotherPerson.FirstName.Returns(anotherFirstName);
            anotherPerson.LastName.Returns(anotherLastName);
            anotherPerson.DateOfBirth.Returns(anotherDateOfBirth);
            anotherPerson.IdentificationNumber.Returns(anotherIdentificationNumber);

            testSubject = new PersonMatcher(new DateOfBirthMatcher(), new IdentificationNumberMatcher(), new NameMatcher());
        }

        [Fact]
        public void ShouldBeAbleToFindANonMatch()
        {
            testSubject.Match(aPerson, anotherPerson).Should().Be(0.0D);
        }

        [Fact]
        public void ShouldBeAbleToMatchByLastName()
        {
            aPerson.LastName.Returns(anotherLastName);

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.4D);
        }

        [Fact]
        public void ShouldBeAbleToMatchByFirstAndLastNames()
        {
            aPerson.LastName.Returns(anotherLastName);
            aPerson.FirstName.Returns(anotherFirstName);

            testSubject.Match(aPerson, anotherPerson).Should().BeApproximately(0.6D, 0.0001D);
        }

        [Fact]
        public void ShouldBeAbleToMatchByFirstLastNamesAndDateOfBirth()
        {
            aPerson.LastName.Returns(anotherLastName);
            aPerson.FirstName.Returns(anotherFirstName);
            aPerson.DateOfBirth.Returns(anotherDateOfBirth);

            testSubject.Match(aPerson, anotherPerson).Should().BeApproximately(1.0D, 0.0001D);
        }

        [Fact]
        public void ShouldBeAbleToMatchByIdentificationNumber()
        {
            aPerson.IdentificationNumber.Returns(anotherIdentificationNumber);

            testSubject.Match(aPerson, anotherPerson).Should().Be(1.0D);
        }

        [Fact]
        public void ShouldBeAbleToMatchByIdentificationNumberAndOtherThings()
        {
            aPerson.IdentificationNumber.Returns(anotherIdentificationNumber);
            aPerson.LastName.Returns(anotherLastName);
            aPerson.FirstName.Returns(anotherFirstName);
            aPerson.DateOfBirth.Returns(anotherDateOfBirth);

            testSubject.Match(aPerson, anotherPerson).Should().BeApproximately(1.0D, 0.0001D);
        }

        [Fact]
        public void ShouldNotBeAbleToMatchNullPeople()
        {
            Action match = () =>
            {
                testSubject.Match(aPerson, anotherPerson);
            };

            aPerson = null;
            match.Should().Throw<ArgumentNullException>();

            aPerson = Substitute.For<IPerson>();
            anotherPerson = null;

            match.Should().Throw<ArgumentNullException>();

            aPerson = null;
            match.Should().Throw<ArgumentNullException>();
        }
    }
}
