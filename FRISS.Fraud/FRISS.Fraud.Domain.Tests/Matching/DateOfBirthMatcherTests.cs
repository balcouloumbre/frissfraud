﻿using FluentAssertions;
using FRISS.Fraud.Domain.Interfaces;
using FRISS.Fraud.Domain.Matching;
using NSubstitute;
using System;
using Xunit;

namespace FRISS.Fraud.Domain.Tests.Matching
{
    public class DateOfBirthMatcherTests
    {
        private readonly IPersonMatcher testSubject;
        private IPerson aPerson;
        private IPerson anotherPerson;
        private readonly DateTime? anDateOfBirth;
        private readonly DateTime? anotherDateOfBirth;

        public DateOfBirthMatcherTests()
        {
            anDateOfBirth = DateTime.Now.AddYears(-30);
            anotherDateOfBirth = DateTime.Now.AddYears(-25);

            aPerson = Substitute.For<IPerson>();
            aPerson.DateOfBirth.Returns(anDateOfBirth);

            anotherPerson = Substitute.For<IPerson>();
            anotherPerson.DateOfBirth.Returns(anDateOfBirth);

            testSubject = new DateOfBirthMatcher();
        }

        [Fact]
        public void ShouldGetA100PercentMatchFromTwoPeopleWithTheSameNumber()
        {
            testSubject.Match(aPerson, anotherPerson).Should().Be(0.4D);
        }

        [Fact]
        public void ShouldGetAZeroPercentMatchFromTwoPeopleWithDifferentNumbers()
        {
            anotherPerson.DateOfBirth.Returns(anotherDateOfBirth);

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.0D);
        }

        [Fact]
        public void ShouldGetAZeroPercentMatchFromTwoPeopleWithUnknownNumbers()
        {
            anotherPerson.DateOfBirth.Returns(default(DateTime?));

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.0D);

            aPerson.DateOfBirth.Returns(default(DateTime?));

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.0D);
        }

        [Fact]
        public void ShouldNotBeAbleToMatchNullPeople()
        {
            Action match = () =>
            {
                testSubject.Match(aPerson, anotherPerson);
            };

            aPerson = null;
            match.Should().Throw<ArgumentNullException>();

            aPerson = Substitute.For<IPerson>();
            anotherPerson = null;

            match.Should().Throw<ArgumentNullException>();

            aPerson = null;
            match.Should().Throw<ArgumentNullException>();
        }
    }
}
