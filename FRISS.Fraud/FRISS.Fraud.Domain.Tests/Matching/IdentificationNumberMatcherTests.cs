﻿using FluentAssertions;
using FRISS.Fraud.Domain.Interfaces;
using FRISS.Fraud.Domain.Matching;
using NSubstitute;
using System;
using Xunit;

namespace FRISS.Fraud.Domain.Tests.Matching
{
    public class IdentificationNumberMatcherTests
    {
        private readonly IPersonMatcher testSubject;
        private IPerson aPerson;
        private IPerson anotherPerson;
        private readonly int? anIdentificationNumber;
        private readonly int? anotherIdentificationNumber;

        public IdentificationNumberMatcherTests()
        {
            anIdentificationNumber = 12345678;
            anotherIdentificationNumber = 23456789;

            aPerson = Substitute.For<IPerson>();
            aPerson.IdentificationNumber.Returns(anIdentificationNumber);

            anotherPerson = Substitute.For<IPerson>();
            anotherPerson.IdentificationNumber.Returns(anIdentificationNumber);

            testSubject = new IdentificationNumberMatcher();
        }

        [Fact]
        public void ShouldGetA100PercentMatchFromTwoPeopleWithTheSameNumber()
        {
            testSubject.Match(aPerson, anotherPerson).Should().Be(1.0D);
        }

        [Fact]
        public void ShouldGetAZeroPercentMatchFromTwoPeopleWithDifferentNumbers()
        {
            anotherPerson.IdentificationNumber.Returns(anotherIdentificationNumber);

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.0D);
        }

        [Fact]
        public void ShouldGetAZeroPercentMatchFromTwoPeopleWithUnknownNumbers()
        {
            anotherPerson.IdentificationNumber.Returns(default(int?));

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.0D);

            aPerson.IdentificationNumber.Returns(default(int?));

            testSubject.Match(aPerson, anotherPerson).Should().Be(0.0D);
        }

        [Fact]
        public void ShouldNotBeAbleToMatchNullPeople()
        {
            Action match = () =>
            {
                testSubject.Match(aPerson, anotherPerson);
            };

            aPerson = null;
            match.Should().Throw<ArgumentNullException>();

            aPerson = Substitute.For<IPerson>();
            anotherPerson = null;

            match.Should().Throw<ArgumentNullException>();

            aPerson = null;
            match.Should().Throw<ArgumentNullException>();
        }
    }
}
