﻿using FluentAssertions;
using FRISS.Fraud.Domain.Matching.Cleanup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FRISS.Fraud.Domain.Tests.Matching.Cleanup
{
    public class StringExtensionsTests
    {
        private string input;
        private string normalizedString;

        [Fact]
        public void ShouldNormalizeAStringWithSpecialCharacters()
        {
            input = "ÂâáàåÀÃãÄäÈËëèÉôÇçÑñÕÒÖõòÍÎÌíýÝöÚú";
            normalizedString = input.RemoveSpecialCharacters();
            normalizedString.Should().Be("AaaaaAAaAaEEeeEoCcNnOOOooIIIiyYoUu");
        }

        [Fact]
        public void ShouldReturnAnEmptyStringWhenOnlySpecialCharactersAreInIt()
        {
            input = @"'`\-(){}:;/!'#[]%";
            normalizedString = input.RemoveSpecialCharacters();
            normalizedString.Should().BeEmpty();
        }

        [Fact]
        public void ShouldRemoveSpecialCharactersFromAString()
        {
            input = @"ABCD\-(){}:;/!'#[]%";
            normalizedString = input.RemoveSpecialCharacters();
            normalizedString.Should().Be("ABCD");
        }

        [Fact]
        public void ShouldNormalizeAndRemoveSpecialCharacters()
        {
            input = @"\-(){}:;/!'#[]%ÂâáàåÀÃãÄäÈËëèôÇçÑñÕÒÖõòÍÎÌíýÝöÚú";
            normalizedString = input.RemoveSpecialCharacters();
            normalizedString.Should().Be("AaaaaAAaAaEEeeoCcNnOOOooIIIiyYoUu");
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("   ")]
        public void ShouldReturnAnEmptyStringForNullOrWhitespaceInputs(string value)
        {
            normalizedString = value.RemoveExtraSpaces();
            normalizedString.Should().BeEmpty();
        }

        [Fact]
        public void ShouldRemoveExtraSpaces()
        {
            input = "  William   Gates the Third  ";
            normalizedString = input.RemoveExtraSpaces();
            normalizedString.Should().Be("William Gates the Third");
        }
    }
}
