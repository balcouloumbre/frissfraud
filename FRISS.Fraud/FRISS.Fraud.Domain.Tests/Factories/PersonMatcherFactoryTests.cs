﻿using FluentAssertions;
using FRISS.Fraud.Domain.Factories;
using FRISS.Fraud.Domain.Interfaces;
using FRISS.Fraud.Domain.Interfaces.Factories;
using Xunit;

namespace FRISS.Fraud.Domain.Tests.Factories
{
    public class PersonMatcherFactoryTests
    {
        private readonly IPersonMatcherFactory testSubject;

        public PersonMatcherFactoryTests()
        {
            testSubject = new PersonMatcherFactory();
        }

        [Fact]
        public void ShouldBeAbleToCreateAPersonMatcher()
        {
            IPersonMatcher personMatcher = testSubject.CreatePersonMatcher();
            personMatcher.Should().NotBeNull();
        }
    }
}
