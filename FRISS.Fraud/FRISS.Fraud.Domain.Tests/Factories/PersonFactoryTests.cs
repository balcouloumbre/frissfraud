﻿using FluentAssertions;
using FRISS.Fraud.Domain.Factories;
using FRISS.Fraud.Domain.Interfaces;
using FRISS.Fraud.Domain.Interfaces.Factories;
using Xunit;

namespace FRISS.Fraud.Domain.Tests.Factories
{
    public class PersonFactoryTests
    {
        private IPersonFactory testSubject;
        private string firstName;
        private string lastName;

        public PersonFactoryTests()
        {
            testSubject = new PersonFactory();
            firstName = "John";
            lastName = "Doe";
        }

        [Fact]
        public void ShouldBeAbleToCreateAPerson()
        {
            IPerson person = testSubject.CreatePerson(firstName, lastName);
            person.Should().NotBeNull();
            person.Id.Should().NotBeEmpty();
            person.FirstName.Should().Be(firstName);
            person.LastName.Should().Be(lastName);
            person.IdentificationNumber.Should().NotHaveValue();
            person.DateOfBirth.Should().NotHaveValue();
        }
    }
}
