﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FRISS.Fraud.Infrastructure
{
    public class SimilarNamesFileReader
    {
        private string SourceFilePath { get; set; }

        public SimilarNamesFileReader(string sourceFilePath)
        {
            SourceFilePath = sourceFilePath;
        }

        public List<KeyValuePair<string, List<string>>> GetSimilarNames()
        {
            List<KeyValuePair<string, List<string>>> similarNames = new();

            if(File.Exists(SourceFilePath))
            {
                using (StreamReader reader = new StreamReader(SourceFilePath))
                using (CsvReader csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    while (csv.Read())
                    {
                        string[] rawRecord = csv.Context.Parser.Record.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
                        string name = rawRecord[0];
                        List<string> similarities = new List<string>();
                        for (int i = 1; i < rawRecord.Length; i++)
                        {
                            similarities.Add(rawRecord[i]);
                        }

                        KeyValuePair<string, List<string>> record = new KeyValuePair<string, List<string>>(name, similarities);
                        similarNames.Add(record);
                    }
                }
            }            

            return similarNames;

        }
    }
}
