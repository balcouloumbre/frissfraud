﻿using FRISS.Fraud.Domain.Interfaces;
using FRISS.Fraud.Domain.Interfaces.Repositories;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace FRISS.Fraud.Infrastructure.Repositories
{
    public class MongoBaseRepository<T, IT> : IRepository<IT>
    where T : class, IT
    where IT : IIdentity
    {
        private string CollectionName { get; set; }
        protected MongoClient Client { get; set; }
        private IMongoDatabase DataBase { get; set; }
        private MongoConfiguration Configuration { get; set; }
        private IMongoCollection<T> Collection { get; set; }

        public MongoBaseRepository(MongoConfiguration configuration, string collectionName)
        {
            this.Configuration = configuration;
            this.CollectionName = collectionName;
            this.Client = new MongoClient(Configuration.ConnectionString);
            this.DataBase = Client.GetDatabase(Configuration.DatabaseName);
        }


        protected IMongoCollection<T> GetCollection()
        {
            if (Collection == null)
            {
                Collection = DataBase.GetCollection<T>(CollectionName);
            }
            return Collection;
        }

        public virtual async Task<IT> GetById(Guid id)
        {
            IAsyncCursor<T> cursor = null;


            cursor = await GetCollection().FindAsync<T>(x => x.Id.Equals(id));


            return await cursor.FirstOrDefaultAsync();
        }

        public virtual async Task Add(IT entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            T concreteEntity = entity as T;

            await GetCollection().InsertOneAsync(concreteEntity);
        }
        public virtual async Task Update(IT entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            T concreteEntity = entity as T;


            await GetCollection().ReplaceOneAsync<T>(x => x.Id.Equals(entity.Id), concreteEntity);
        }


        public virtual async Task DeleteAll()
        {
            await GetCollection().DeleteManyAsync<T>(x => 1 == 1);
        }
    }
}
