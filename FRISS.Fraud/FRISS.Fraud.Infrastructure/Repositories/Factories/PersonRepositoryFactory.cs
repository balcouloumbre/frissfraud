﻿using FRISS.Fraud.Domain.Interfaces.Repositories;
using FRISS.Fraud.Domain.Interfaces.Repositories.Factories;

namespace FRISS.Fraud.Infrastructure.Repositories.Factories
{
    public class PersonRepositoryFactory : IPersonRepositoryFactory
    {
        private MongoConfiguration Configuration { get; set; }
        public PersonRepositoryFactory(MongoConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IPersonRepository CreateRepository()
        {
            return new PersonRepository(Configuration);
        }
    }
}
