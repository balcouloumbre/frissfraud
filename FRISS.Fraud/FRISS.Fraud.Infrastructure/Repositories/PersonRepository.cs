﻿using FRISS.Fraud.Domain;
using FRISS.Fraud.Domain.Interfaces;
using FRISS.Fraud.Domain.Interfaces.Repositories;
using MongoDB.Bson.Serialization;
using System;

namespace FRISS.Fraud.Infrastructure.Repositories
{
    public class PersonRepository : MongoBaseRepository<Person, IPerson>, IPersonRepository
    {
        static PersonRepository()
        {
            RegisterClassMap();
        }

        public PersonRepository(MongoConfiguration configuration) : base(configuration, "People")
        {
        }

        private static void RegisterClassMap()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(Person)))
            {
                BsonClassMap.RegisterClassMap<Person>(classMap =>
                {
                    classMap.SetIgnoreExtraElements(true);
                    classMap.AutoMap();

                    classMap.MapIdProperty(x => x.Id);
                    classMap.MapProperty<string>(x => x.FirstName);
                    classMap.MapProperty<string>(x => x.LastName);
                    classMap.MapProperty<DateTime?>(x => x.DateOfBirth);
                    classMap.MapProperty<int?>(x => x.IdentificationNumber);
                });
            }
        }
    }
}
