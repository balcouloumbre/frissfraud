﻿namespace FRISS.Fraud.Infrastructure
{
    public class MongoConfiguration
    {
        public MongoConfiguration(string connectionString, string databaseName)
        {
            ConnectionString = connectionString;
            DatabaseName = databaseName;
        }

        public string ConnectionString { get; }
        public string DatabaseName { get; }
    }
}
