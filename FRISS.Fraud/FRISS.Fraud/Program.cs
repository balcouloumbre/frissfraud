using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;

namespace FRISS.Fraud.Api
{
    public class Program
    {
        public static int Main(string[] args)
        {
            try
            {
                IConfigurationBuilder builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .AddEnvironmentVariables();
                IConfigurationRoot configuration = builder.Build();

                CreateHostBuilder(args, configuration).Build().Run();

                return 0;
            }
            catch(Exception ex)
            {
                Serilog.Log.Fatal(ex, "Host crashed at runtime.");
                return 1;
            }
            finally
            {
                Serilog.Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args, IConfigurationRoot configuration)
        {
            IConfigurationSection logsDbSection = configuration.GetSection("ConnectionStrings:logsDb");
            string logsDbConnectionString = logsDbSection.Value;

            return Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var settings = config.Build();
                    Serilog.Log.Logger = new LoggerConfiguration()
                        .Enrich.FromLogContext()
                        .WriteTo.MongoDB(
                            databaseUrl: logsDbConnectionString,
                            collectionName: "Logs")
                        .WriteTo.Console()
                        .CreateLogger();
                })
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseConfiguration(configuration)
                    .UseStartup<Startup>();
                });
        }            
    }
}
