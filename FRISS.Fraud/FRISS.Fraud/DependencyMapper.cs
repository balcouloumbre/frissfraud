﻿using FRISS.Fraud.Domain.Factories;
using FRISS.Fraud.Domain.Interfaces.Factories;
using FRISS.Fraud.Domain.Interfaces.Repositories.Factories;
using FRISS.Fraud.Infrastructure;
using FRISS.Fraud.Infrastructure.Repositories.Factories;
using FRISS.Fraud.Service;
using FRISS.Fraud.Service.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace FRISS.Fraud.Api
{
    public class DependencyMapper
    {
        public static void MapDependencies(IServiceCollection services, MongoConfiguration mongoConfiguration)
        {
            MapDependencies(services, mongoConfiguration, new List<KeyValuePair<string, List<string>>>());
        }

        public static void MapDependencies(IServiceCollection services, MongoConfiguration mongoConfiguration, List<KeyValuePair<string, List<string>>> similarNames)
        {
            MapFactories(services, mongoConfiguration, similarNames);
            MapServices(services);
        }

        private static void MapFactories(IServiceCollection services, MongoConfiguration mongoConfiguration, List<KeyValuePair<string, List<string>>> similarNames)
        {
            #region Entity Factories

            services.AddScoped<IPersonFactory, PersonFactory>();
            services.AddSingleton<IPersonMatcherFactory, PersonMatcherFactory>(f => new PersonMatcherFactory(similarNames));

            #endregion

            #region Repository Factories

            services.AddScoped<IPersonRepositoryFactory, PersonRepositoryFactory>(r => new PersonRepositoryFactory(mongoConfiguration));

            #endregion
        }

        private static void MapServices(IServiceCollection services)
        {
            services.AddScoped<IPersonService, PersonService>();
        }
    }
}
