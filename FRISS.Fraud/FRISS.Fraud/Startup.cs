using FRISS.Fraud.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;

namespace FRISS.Fraud.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            IConfigurationSection dbConnectionSettings = Configuration.GetSection("ConnectionStrings:dbHost");
            string connectionString = dbConnectionSettings.Value;

            IConfigurationSection dbNameSettings = Configuration.GetSection("ConnectionStrings:db");
            string dbName = dbNameSettings.Value;
            MongoConfiguration mongoConfiguration = new MongoConfiguration(connectionString, dbName);

            IConfigurationSection similarNamesSettings = Configuration.GetSection("SimilarNamesFile");
            string similarNamesFile = similarNamesSettings.Value;

            SimilarNamesFileReader similarNamesFileReader = new SimilarNamesFileReader(similarNamesFile);
            List<KeyValuePair<string, List<string>>> similarNames = similarNamesFileReader.GetSimilarNames();

            DependencyMapper.MapDependencies(services, mongoConfiguration, similarNames);

            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "FRISS.Fraud.Api", Version = "v1" });
            });

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "FRISS.Fraud v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
