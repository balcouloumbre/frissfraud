﻿using FRISS.Fraud.Service.Interfaces;
using FRISS.Fraud.Service.Models.PersonService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace FRISS.Fraud.Api.Controllers
{
    [Route("/")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly ILogger<PersonController> logger;
        private readonly IPersonService service;

        public PersonController(IPersonService personService, ILogger<PersonController> logProvider)
        {
            service = personService;
            logger = logProvider;
        }

        [HttpGet("status")]
        public ActionResult Status()
        {
            string serviceStatus = "Service is up and running.";

            logger.LogInformation(serviceStatus);
            return Ok(serviceStatus);
        }

        [HttpGet("get")]
        public async Task<ActionResult<PersonDto>> Get(Guid id)
        {
            try
            {
                PersonDto response = await service.Get(id);
                if (response == default)
                {
                    logger.LogInformation($"Person not found for id: {id}");
                    return NotFound();
                }
                else
                {
                    logger.LogInformation($"Person found for id: {id}");
                    return Ok(response);
                }
            }
            catch (ApplicationException ex)
            {
                logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("create")]
        public async Task<ActionResult<Guid>> CreatePerson(CreatePersonRequest request)
        {
            try
            {
                Guid id = await service.CreatePerson(request);
                logger.LogInformation($"Person created with id: {id}");
                return Ok(id);
            }
            catch (ApplicationException ex)
            {
                logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("match")]
        public async Task<ActionResult<MatchResultDto>> MatchPeople(MatchRequest request)
        {
            try
            {
                MatchResultDto result = await service.MatchPeople(request);
                if (result == default)
                {
                    logger.LogInformation($"Unable to match people for ids: {request.FirstPersonId} and {request.SecondPersonId}.");
                    return NotFound();
                }
                else
                {
                    logger.LogInformation($"Match successful for ids: {request.FirstPersonId} and {request.SecondPersonId}.");
                    return Ok(result);
                }
            }
            catch (ApplicationException ex)
            {
                logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
