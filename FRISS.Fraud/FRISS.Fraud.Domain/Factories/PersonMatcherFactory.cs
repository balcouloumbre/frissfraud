﻿using FRISS.Fraud.Domain.Interfaces;
using FRISS.Fraud.Domain.Interfaces.Factories;
using FRISS.Fraud.Domain.Matching;
using System.Collections.Generic;
using System.Linq;

namespace FRISS.Fraud.Domain.Factories
{
    public class PersonMatcherFactory : IPersonMatcherFactory
    {
        public PersonMatcherFactory()
        {

        }

        public PersonMatcherFactory(List<KeyValuePair<string, List<string>>> similarNames)
        {
            foreach(KeyValuePair<string, List<string>> nameSimilarity in similarNames ?? Enumerable.Empty<KeyValuePair<string, List<string>>>())
            {
                NameMatcher.AddNameSimilarity(nameSimilarity.Key, nameSimilarity.Value);
            }
        }

        public IPersonMatcher CreatePersonMatcher()
        {
            return new PersonMatcher(new DateOfBirthMatcher(), new IdentificationNumberMatcher(), new NameMatcher());
        }
    }
}
