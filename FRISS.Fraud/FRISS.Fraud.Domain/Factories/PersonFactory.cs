﻿using FRISS.Fraud.Domain.Interfaces;
using FRISS.Fraud.Domain.Interfaces.Factories;

namespace FRISS.Fraud.Domain.Factories
{
    public class PersonFactory : IPersonFactory
    {
        public IPerson CreatePerson(string firstName, string lastName)
        {
            return new Person(firstName, lastName);
        }
    }
}
