﻿using FRISS.Fraud.Domain.Interfaces;
using System;

namespace FRISS.Fraud.Domain
{
    public class Person : IPerson
    {
        public Guid Id { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public int? IdentificationNumber { get; private set; }
        public DateTime? DateOfBirth { get; private set; }

        public Person(string firstName, string lastName)
        {
            if (firstName == null)
            {
                throw new ArgumentNullException(firstName);
            }
            if (lastName == null)
            {
                throw new ArgumentNullException(lastName);
            }

            if (string.IsNullOrWhiteSpace(firstName))
            {
                throw new ArgumentException(firstName);
            }

            if (string.IsNullOrWhiteSpace(lastName))
            {
                throw new ArgumentException(lastName);
            }

            FirstName = firstName;
            LastName = lastName;
            Id = Guid.NewGuid();
        }

        public void SetIdentificationNumber(int identificationNumber)
        {
            if (identificationNumber <= 0)
            {
                throw new ArgumentException($"{nameof(identificationNumber)} should be greater than 0.");
            }

            IdentificationNumber = identificationNumber;
        }

        public void SetDateOfBirth(DateTime dateOfBirth)
        {
            if (dateOfBirth.Date > DateTime.Now.Date)
            {
                throw new ArgumentException($"{nameof(dateOfBirth)} should not be a date in the future.");
            }

            DateOfBirth = dateOfBirth;
        }
    }
}
