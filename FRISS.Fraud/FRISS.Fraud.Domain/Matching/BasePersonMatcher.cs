﻿using FRISS.Fraud.Domain.Interfaces;
using System;

namespace FRISS.Fraud.Domain.Matching
{
    public abstract class BasePersonMatcher : IPersonMatcher
    {
        public double Match(IPerson aPerson, IPerson anotherPerson)
        {
            if (aPerson is null)
            {
                throw new ArgumentNullException(nameof(aPerson));
            }

            if (anotherPerson is null)
            {
                throw new ArgumentNullException(nameof(anotherPerson));
            }

            return DoMatch(aPerson, anotherPerson);
        }

        protected abstract double DoMatch(IPerson aPerson, IPerson anotherPerson);
    }
}
