﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FRISS.Fraud.Domain.Matching.Cleanup
{
    public static class StringExtensions
    {
        private static readonly String[] SpecialCharacters = { "-", "/", @"\", "_", "#", "`", "'", "|", ":", ";", "(", ")", "{", "}", "[", "]", "*", "%", "!", ".", "$" };

        public static string RemoveSpecialCharacters(this string text)
        {
            if (text != null)
            {
                foreach (string diacritics in SpecialCharacters)
                {
                    text = text.Replace(diacritics, string.Empty);
                }


                var normalizedString = text.Normalize(NormalizationForm.FormD);
                var stringBuilder = new StringBuilder();

                foreach (var c in normalizedString)
                {
                    var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                    if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                    {
                        stringBuilder.Append(c);
                    }
                }

                return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
            }
            else
            {
                return string.Empty;
            }
        }

        public static string RemoveExtraSpaces(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return string.Empty;
            }

            StringBuilder tempBuilder = new StringBuilder(text.Length);

            string textCopy = text;
            bool inSpaces = false;
            tempBuilder.Length = 0;

            for (int i = 0; i < text.Length; ++i)
            {
                char c = textCopy[i];

                if (inSpaces)
                {
                    if (c != ' ')
                    {
                        inSpaces = false;
                        tempBuilder.Append(c);
                    }
                }
                else if (c == ' ')
                {
                    inSpaces = true;
                    tempBuilder.Append(' ');
                }
                else
                    tempBuilder.Append(c);
            }

            return tempBuilder.ToString().Trim();
        }
    }
}
