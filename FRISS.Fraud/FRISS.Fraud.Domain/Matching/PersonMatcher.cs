﻿using FRISS.Fraud.Domain.Interfaces;
using System;

namespace FRISS.Fraud.Domain.Matching
{
    public class PersonMatcher : BasePersonMatcher
    {
        private IPersonMatcher DateOfBirthMatcher { get; set; }
        private IPersonMatcher IdentificationNumberMatcher { get; set; }
        private IPersonMatcher NameMatcher { get; set; }

        public PersonMatcher(IPersonMatcher dateOfBirthMatcher, IPersonMatcher identificationNumberMatcher, IPersonMatcher nameMatcher)
        {
            DateOfBirthMatcher = dateOfBirthMatcher ?? throw new ArgumentNullException(nameof(dateOfBirthMatcher));
            IdentificationNumberMatcher = identificationNumberMatcher ?? throw new ArgumentNullException(nameof(identificationNumberMatcher));
            NameMatcher = nameMatcher ?? throw new ArgumentNullException(nameof(nameMatcher));
        }

        protected override double DoMatch(IPerson aPerson, IPerson anotherPerson)
        {
            double match = 0.0D;

            match += IdentificationNumberMatcher.Match(aPerson, anotherPerson);
            match += DateOfBirthMatcher.Match(aPerson, anotherPerson);
            match += NameMatcher.Match(aPerson, anotherPerson);

            return Math.Min(match, 1.0D);
        }
    }
}
