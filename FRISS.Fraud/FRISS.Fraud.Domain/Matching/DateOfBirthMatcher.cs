﻿using FRISS.Fraud.Domain.Interfaces;

namespace FRISS.Fraud.Domain.Matching
{
    public class DateOfBirthMatcher : BasePersonMatcher
    {
        protected override double DoMatch(IPerson aPerson, IPerson anotherPerson)
        {
            double match = 0.0D;

            if (aPerson.DateOfBirth.HasValue && anotherPerson.DateOfBirth.HasValue
                && aPerson.DateOfBirth != default && anotherPerson.DateOfBirth != default
                && aPerson.DateOfBirth.Value.Date.Equals(anotherPerson.DateOfBirth.Value.Date))
            {
                match = 0.4D;
            }

            return match;
        }
    }
}
