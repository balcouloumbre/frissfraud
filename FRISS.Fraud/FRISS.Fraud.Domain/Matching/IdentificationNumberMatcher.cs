﻿using FRISS.Fraud.Domain.Interfaces;

namespace FRISS.Fraud.Domain.Matching
{
    public class IdentificationNumberMatcher : BasePersonMatcher
    {
        protected override double DoMatch(IPerson aPerson, IPerson anotherPerson)
        {
            double match = 0.0D;

            if (aPerson.IdentificationNumber.HasValue && anotherPerson.IdentificationNumber.HasValue
                && aPerson.IdentificationNumber != default && anotherPerson.IdentificationNumber != default
                && aPerson.IdentificationNumber.Value.Equals(anotherPerson.IdentificationNumber.Value))
            {
                match = 1.0D;
            }

            return match;
        }
    }
}
