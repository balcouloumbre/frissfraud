﻿using FRISS.Fraud.Domain.Interfaces;
using FRISS.Fraud.Domain.Matching.Cleanup;
using Similarity;
using System.Collections.Generic;

namespace FRISS.Fraud.Domain.Matching
{
    //TODO: add similar first name matching.
    public class NameMatcher : BasePersonMatcher
    {
        private const decimal STRINGSIMILARITYPERCENTAGE = 0.60M;
        private static Dictionary<string, HashSet<string>> SimilarNames { get; set; }

        static NameMatcher()
        {
            SimilarNames = new();
        }

        public static void AddNameSimilarity(string name, List<string> similarities)
        {
            string upperCaseName = name.RemoveSpecialCharacters().RemoveExtraSpaces().ToUpper();
            HashSet<string> similaritiesToUpper = new HashSet<string>();
            foreach(string similarity in similarities)
            {
                similaritiesToUpper.Add(similarity.RemoveSpecialCharacters().RemoveExtraSpaces().ToUpper());
            }

            if(!SimilarNames.ContainsKey(upperCaseName))
            {
                SimilarNames.Add(upperCaseName, similaritiesToUpper);
            }
            else
            {
                SimilarNames[upperCaseName] = similaritiesToUpper;
            }
        }

        protected override double DoMatch(IPerson aPerson, IPerson anotherPerson)
        {
            double match = 0.0D;

            match += MatchFirstName(aPerson, anotherPerson);
            match += MatchLastName(aPerson, anotherPerson);

            return match;
        }

        private double MatchFirstName(IPerson aPerson, IPerson anotherPerson)
        {
            double match = 0.0D;            

            if (!string.IsNullOrWhiteSpace(aPerson.FirstName) && !string.IsNullOrWhiteSpace(anotherPerson.FirstName))
            {
                string aPersonCleanFirstName = aPerson.FirstName.RemoveSpecialCharacters().RemoveExtraSpaces().ToUpper();
                string anotherPersonCleanFirstName = anotherPerson.FirstName.RemoveSpecialCharacters().RemoveExtraSpaces().ToUpper();
                if(aPersonCleanFirstName.Equals(anotherPersonCleanFirstName))
                {
                    match = 0.2D;
                }
                else if(MatchFirstNameAndInitial(aPersonCleanFirstName, anotherPersonCleanFirstName))
                {
                    match = 0.15D;
                }
                else if(MatchSimilarFirstName(aPersonCleanFirstName, anotherPersonCleanFirstName))
                {
                    match = 0.15D;
                }
                else if(MatchSimilarString(aPersonCleanFirstName, anotherPersonCleanFirstName))
                {
                    match = 0.15D;
                }
            }

            return match;
        }

        private bool MatchSimilarString(string aFirstName, string anotherFirstName)
        {
            decimal similarity = StringSimilarity.Calculate(aFirstName, anotherFirstName);

            return similarity >= STRINGSIMILARITYPERCENTAGE;
        }

        private bool MatchFirstNameAndInitial(string aFirstName, string anotherFirstName)
        {
            string[] aFirstNameComponents = aFirstName.Split(" ");
            string[] anotherFirstNameComponents = anotherFirstName.Split(" ");

            if (aFirstNameComponents.Length.Equals(anotherFirstNameComponents.Length))
            {
                bool match = true;
                for (int i = 0; i < aFirstNameComponents.Length; i++)
                {
                    if(!(aFirstNameComponents[i].Length.Equals(1) && anotherFirstNameComponents[i].StartsWith(aFirstNameComponents[i]))
                        && !(anotherFirstNameComponents[i].Length.Equals(1) && aFirstNameComponents[i].StartsWith(anotherFirstNameComponents[i])))
                    {
                        match = false;
                    }
                }

                if (match)
                {
                    return true;
                }                    
            }

            return false;            
        }

        private bool MatchSimilarFirstName(string aFirstName, string anotherFirstName)
        {
            if (SimilarNames.ContainsKey(aFirstName) && SimilarNames[aFirstName].Contains(anotherFirstName))
            {
                return true;
            }
            else if(SimilarNames.ContainsKey(anotherFirstName) && SimilarNames[anotherFirstName].Contains(aFirstName))
            {
                return true;
            }

            return false;
        }

        private double MatchLastName(IPerson aPerson, IPerson anotherPerson)
        {
            double match = 0.0D;

            if (!string.IsNullOrWhiteSpace(aPerson.LastName) && !string.IsNullOrWhiteSpace(anotherPerson.LastName))
            {
                string aPersonCleanLastName = aPerson.LastName.RemoveSpecialCharacters().RemoveExtraSpaces().ToUpper();
                string anotherPersonCleanLastName = anotherPerson.LastName.RemoveSpecialCharacters().RemoveExtraSpaces().ToUpper();

                if (aPersonCleanLastName.Equals(anotherPersonCleanLastName))
                {
                    match = 0.4D;
                }
            }           

            return match;
        }
    }
}
