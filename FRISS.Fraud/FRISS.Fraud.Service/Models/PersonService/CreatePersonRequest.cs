﻿using System;

namespace FRISS.Fraud.Service.Models.PersonService
{
    public class CreatePersonRequest
    {
        public string FirstName { get; set; }
        public string LastNAme { get; set; }
        public int? IdentificationNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
    }
}
