﻿using System;

namespace FRISS.Fraud.Service.Models.PersonService
{
    public class PersonDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IdentificationNumber { get; set; }
        public string DateOfBirth { get; set; }
    }
}
