﻿using System;

namespace FRISS.Fraud.Service.Models.PersonService
{
    public class MatchRequest
    {
        public Guid FirstPersonId { get; set; }
        public Guid SecondPersonId { get; set; }
    }
}
