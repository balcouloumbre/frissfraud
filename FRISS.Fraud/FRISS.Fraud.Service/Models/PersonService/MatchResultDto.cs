﻿namespace FRISS.Fraud.Service.Models.PersonService
{
    public class MatchResultDto
    {
        public PersonDto FirstPerson { get; set; }
        public PersonDto SecondPerson { get; set; }
        public string Match { get; set; }
    }
}
