﻿using FRISS.Fraud.Domain.Interfaces;
using FRISS.Fraud.Domain.Interfaces.Factories;
using FRISS.Fraud.Domain.Interfaces.Repositories;
using FRISS.Fraud.Domain.Interfaces.Repositories.Factories;
using FRISS.Fraud.Service.Interfaces;
using FRISS.Fraud.Service.Models.PersonService;
using System;
using System.Threading.Tasks;

namespace FRISS.Fraud.Service
{
    public class PersonService : IPersonService
    {
        private readonly IPersonFactory _personFactory;
        private readonly IPersonRepositoryFactory _personRepositoryFactory;
        private readonly IPersonMatcherFactory _personMatcherFactory;

        public PersonService(IPersonFactory personFactory, IPersonRepositoryFactory personRepositoryFactory,
            IPersonMatcherFactory personMatcherFactory)
        {
            _personFactory = personFactory;
            _personRepositoryFactory = personRepositoryFactory;
            _personMatcherFactory = personMatcherFactory;
        }

        public async Task<Guid> CreatePerson(CreatePersonRequest request)
        {
            Guid id;
            try
            {
                IPerson person = _personFactory.CreatePerson(request.FirstName, request.LastNAme);

                if (request.DateOfBirth.HasValue)
                {
                    person.SetDateOfBirth(request.DateOfBirth.Value);
                }

                if (request.IdentificationNumber.HasValue)
                {
                    person.SetIdentificationNumber(request.IdentificationNumber.Value);
                }

                IPersonRepository personRepository = _personRepositoryFactory.CreateRepository();
                await personRepository.Add(person);

                id = person.Id;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating Person.", ex);
            }

            return id;
        }

        public async Task<PersonDto> Get(Guid id)
        {
            PersonDto dto = default;

            try
            {
                IPersonRepository personRepository = _personRepositoryFactory.CreateRepository();
                IPerson person = await personRepository.GetById(id);

                if (person != default)
                {
                    dto = MapPersonToDto(person);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error getting Person.", ex);
            }

            return dto;
        }

        private PersonDto MapPersonToDto(IPerson person)
        {
            return new PersonDto()
            {
                Id = person.Id,
                FirstName = person.FirstName,
                LastName = person.LastName,
                IdentificationNumber = person.IdentificationNumber.HasValue ? person.IdentificationNumber.Value.ToString() : "Unknown",
                DateOfBirth = person.DateOfBirth.HasValue ? person.DateOfBirth.Value.ToString("dd/MM/yyyy") : "Unknown",
            };
        }

        public async Task<MatchResultDto> MatchPeople(MatchRequest request)
        {
            MatchResultDto dto = default;

            try
            {
                IPersonRepository personRepository = _personRepositoryFactory.CreateRepository();
                IPerson firstPerson = await personRepository.GetById(request.FirstPersonId);
                IPerson secondPerson = await personRepository.GetById(request.SecondPersonId);

                if (firstPerson != default && secondPerson != default)
                {
                    IPersonMatcher personMatcher = _personMatcherFactory.CreatePersonMatcher();
                    double match = personMatcher.Match(firstPerson, secondPerson);

                    dto = new MatchResultDto()
                    {
                        FirstPerson = MapPersonToDto(firstPerson),
                        SecondPerson = MapPersonToDto(secondPerson),
                        Match = string.Format("{0:N2}%", match * 100)
                    };
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error matching people.", ex);
            }

            return dto;
        }
    }
}
