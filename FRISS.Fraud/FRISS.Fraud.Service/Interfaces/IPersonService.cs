﻿using FRISS.Fraud.Service.Models.PersonService;
using System;
using System.Threading.Tasks;

namespace FRISS.Fraud.Service.Interfaces
{
    public interface IPersonService
    {
        Task<Guid> CreatePerson(CreatePersonRequest request);
        Task<MatchResultDto> MatchPeople(MatchRequest request);
        Task<PersonDto> Get(Guid id);
    }
}
