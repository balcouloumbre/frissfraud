﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FRISS.Fraud.Infrastructure.Tests
{
    public class SimilarNamesFileReaderTests
    {
        private SimilarNamesFileReader testSubject;
        private string filePath;

        public SimilarNamesFileReaderTests()
        {
            filePath = "Files\\test-names.csv";
            testSubject = new SimilarNamesFileReader(filePath);
        }

        [Fact]
        public void ShouldBeAbleToReadACsvOfNames()
        {
            List <KeyValuePair<string, List<string>>> similarNames = testSubject.GetSimilarNames();
            similarNames.Should().NotBeNull();
            similarNames.Should().HaveCount(2);

            similarNames.Any(x => x.Key.Equals("aaron") && x.Value.Count.Equals(3)).Should().BeTrue();
            similarNames.Any(x => x.Key.Equals("abbigail") && x.Value.Count.Equals(5)).Should().BeTrue();
        }

        [Fact]
        public void ShouldBeAbleToReadAFileThatDoesNotExist()
        {
            filePath = "Files\\invalid.csv";
            testSubject = new SimilarNamesFileReader(filePath);

            List<KeyValuePair<string, List<string>>> similarNames = testSubject.GetSimilarNames();
            similarNames.Should().NotBeNull();
            similarNames.Should().BeEmpty();
        }
    }
}
