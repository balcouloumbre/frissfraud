﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;

namespace FRISS.Fraud.Infrastructure.Tests
{
    public class MongoIntegrationFixture : IDisposable
    {
        public MongoConfiguration DbSettings { get; }

        public MongoIntegrationFixture()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.InfrastructureTest.json")
                .Build();

            var connectionString = config.GetConnectionString("db");
            var dbName = $"test_db_{Guid.NewGuid()}";

            DbSettings = new MongoConfiguration(connectionString, dbName);
        }

        #region IDisposable
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    MongoClient client = new MongoClient(this.DbSettings.ConnectionString);
                    client.DropDatabase(this.DbSettings.DatabaseName);
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~MongoIntegrationFixture()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
