﻿using FluentAssertions;
using FRISS.Fraud.Domain;
using FRISS.Fraud.Domain.Interfaces;
using FRISS.Fraud.Infrastructure.Repositories;
using System;
using System.Threading.Tasks;
using Xunit;

namespace FRISS.Fraud.Infrastructure.Tests.Repositories
{
    public class PersonRepositoryTests : IClassFixture<MongoIntegrationFixture>
    {
        private readonly MongoIntegrationFixture _mongoFixture;

        private readonly PersonRepository testSubject;

        private readonly IPerson aPerson;


        public PersonRepositoryTests(MongoIntegrationFixture fixture)
        {
            _mongoFixture = fixture;

            aPerson = new Person("John", "Doe");
            aPerson.SetIdentificationNumber(123456);
            aPerson.SetDateOfBirth(DateTime.Now.AddYears(-20));

            testSubject = new PersonRepository(_mongoFixture.DbSettings);

            CleanupData().GetAwaiter().GetResult();
        }

        private async Task CleanupData()
        {
            await testSubject.DeleteAll();
        }

        [Fact]
        public async Task ShouldBeAbleToAddAndGetAPerson()
        {
            IPerson aPersonFromTheDb = await testSubject.GetById(aPerson.Id);
            aPersonFromTheDb.Should().BeNull();

            await testSubject.Add(aPerson);

            aPersonFromTheDb = await testSubject.GetById(aPerson.Id);
            aPersonFromTheDb.Should().NotBeNull();
            aPersonFromTheDb.Id.Should().Be(aPerson.Id);
            aPersonFromTheDb.FirstName.Should().Be(aPerson.FirstName);
            aPersonFromTheDb.LastName.Should().Be(aPerson.LastName);
            aPersonFromTheDb.DateOfBirth.Value.Date.Should().Be(aPerson.DateOfBirth.Value.Date);
            aPersonFromTheDb.IdentificationNumber.Should().Be(aPerson.IdentificationNumber);
        }

        [Fact]
        public async Task ShouldBeAbleToUpdateAPerson()
        {
            await testSubject.Add(aPerson);

            IPerson aPersonFromTheDb = await testSubject.GetById(aPerson.Id);

            aPersonFromTheDb.Should().NotBeNull();
            aPersonFromTheDb.Id.Should().Be(aPerson.Id);
            aPersonFromTheDb.FirstName.Should().Be(aPerson.FirstName);
            aPersonFromTheDb.LastName.Should().Be(aPerson.LastName);
            aPersonFromTheDb.DateOfBirth.Value.Date.Should().Be(aPerson.DateOfBirth.Value.Date);
            aPersonFromTheDb.IdentificationNumber.Should().Be(aPerson.IdentificationNumber);

            int anotherIdentificationNumber = 2345678;
            anotherIdentificationNumber.Should().NotBe(aPerson.IdentificationNumber);

            aPerson.SetIdentificationNumber(2345678);

            await testSubject.Update(aPerson);
            aPersonFromTheDb = await testSubject.GetById(aPerson.Id);
            aPersonFromTheDb.Should().NotBeNull();
            aPersonFromTheDb.Id.Should().Be(aPerson.Id);
            aPersonFromTheDb.FirstName.Should().Be(aPerson.FirstName);
            aPersonFromTheDb.LastName.Should().Be(aPerson.LastName);
            aPersonFromTheDb.DateOfBirth.Value.Date.Should().Be(aPerson.DateOfBirth.Value.Date);
            aPersonFromTheDb.IdentificationNumber.Should().Be(anotherIdentificationNumber);
        }
    }
}
