﻿using FluentAssertions;
using FRISS.Fraud.Infrastructure.Repositories;
using FRISS.Fraud.Infrastructure.Repositories.Factories;
using Xunit;

namespace FRISS.Fraud.Infrastructure.Tests.Repositories.Factories
{
    public class PersonRepositoryFactoryTests : IClassFixture<MongoIntegrationFixture>
    {
        private readonly MongoIntegrationFixture _mongoFixture;

        private readonly PersonRepositoryFactory testSubject;

        public PersonRepositoryFactoryTests(MongoIntegrationFixture fixture)
        {
            _mongoFixture = fixture;

            testSubject = new PersonRepositoryFactory(_mongoFixture.DbSettings);
        }

        [Fact]
        public void ShouldBeAbleToCreateARepository()
        {
            PersonRepository repository = testSubject.CreateRepository() as PersonRepository;
            repository.Should().NotBeNull();
        }
    }
}
