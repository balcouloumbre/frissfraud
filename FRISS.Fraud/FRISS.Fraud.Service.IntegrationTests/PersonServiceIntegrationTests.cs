﻿using FluentAssertions;
using FRISS.Fraud.Domain.Interfaces.Repositories;
using FRISS.Fraud.Domain.Interfaces.Repositories.Factories;
using FRISS.Fraud.Service.Interfaces;
using FRISS.Fraud.Service.Models.PersonService;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using Xunit;

namespace FRISS.Fraud.Service.IntegrationTests
{
    public class PersonServiceIntegrationTests : IClassFixture<BaseIntegrationFixture>
    {
        private readonly BaseIntegrationFixture _integrationFixture;

        private readonly IPersonService testSubject;

        private readonly IPersonRepositoryFactory personRepositoryFactory;

        private readonly CreatePersonRequest createPersonRequest;
        private readonly CreatePersonRequest createAnotherPersonRequest;


        public PersonServiceIntegrationTests(BaseIntegrationFixture fixture)
        {
            createPersonRequest = new CreatePersonRequest()
            {
                FirstName = "John",
                LastNAme = "Doe"
            };

            createAnotherPersonRequest = new CreatePersonRequest()
            {
                FirstName = "Jane",
                LastNAme = "Doe"
            };

            _integrationFixture = fixture;

            personRepositoryFactory = _integrationFixture.ServiceProvider.GetService<IPersonRepositoryFactory>();

            testSubject = _integrationFixture.ServiceProvider.GetService<IPersonService>();

            CleanupData().GetAwaiter().GetResult();
        }

        private async Task CleanupData()
        {
            IPersonRepository personRepository = personRepositoryFactory.CreateRepository();
            await personRepository.DeleteAll();
        }

        [Fact]
        public async Task ShouldBeAbleToCreateAndGetAPerson()
        {
            Guid id = await testSubject.CreatePerson(createPersonRequest);
            id.Should().NotBeEmpty();

            PersonDto dto = await testSubject.Get(id);
            dto.Should().NotBeNull();
            dto.Id.Should().Be(id);
            dto.FirstName.Should().Be(createPersonRequest.FirstName);
            dto.LastName.Should().Be(createPersonRequest.LastNAme);
            dto.DateOfBirth.Should().Be("Unknown");
            dto.IdentificationNumber.Should().Be("Unknown");
        }

        [Fact]
        public async Task ShouldBeAbleToCreateAndGetAPersonWithFullData()
        {
            createPersonRequest.IdentificationNumber = 123456;
            createPersonRequest.DateOfBirth = DateTime.Now.AddYears(-30);

            Guid id = await testSubject.CreatePerson(createPersonRequest);
            id.Should().NotBeEmpty();

            PersonDto dto = await testSubject.Get(id);
            dto.Should().NotBeNull();
            dto.Id.Should().Be(id);
            dto.FirstName.Should().Be(createPersonRequest.FirstName);
            dto.LastName.Should().Be(createPersonRequest.LastNAme);
            dto.DateOfBirth.Should().Be(createPersonRequest.DateOfBirth.Value.ToString("dd/MM/yyyy"));
            dto.IdentificationNumber.Should().Be(createPersonRequest.IdentificationNumber.ToString());
        }

        [Fact]
        public async Task ShouldBeAbleToGetAPersonThatDoesNotExist()
        {
            PersonDto dto = await testSubject.Get(Guid.NewGuid());
            dto.Should().BeNull();
        }

        [Fact]
        public void ShouldNotBeAbleToCreateAPersonWithoutAName()
        {
            createPersonRequest.FirstName = string.Empty;
            createPersonRequest.LastNAme = string.Empty;

            Func<Task> create = async () => { await testSubject.CreatePerson(createPersonRequest); };

            create.Should().Throw<ApplicationException>().WithInnerException<ArgumentException>();
        }

        [Fact]
        public async Task ShouldBeAbleToMatchTwoPeople()
        {
            Guid aPersonId = await testSubject.CreatePerson(createPersonRequest);
            Guid anotherPersonId = await testSubject.CreatePerson(createAnotherPersonRequest);

            MatchRequest request = new MatchRequest()
            {
                FirstPersonId = aPersonId,
                SecondPersonId = anotherPersonId
            };

            MatchResultDto resultDto = await testSubject.MatchPeople(request);
            resultDto.Should().NotBeNull();
            resultDto.Match.Should().Be($"40.00%");

            PersonDto firstPersonDto = resultDto.FirstPerson;
            firstPersonDto.Should().NotBeNull();
            firstPersonDto.Id.Should().Be(aPersonId);
            firstPersonDto.FirstName.Should().Be(createPersonRequest.FirstName);
            firstPersonDto.LastName.Should().Be(createPersonRequest.LastNAme);
            firstPersonDto.DateOfBirth.Should().Be("Unknown");
            firstPersonDto.IdentificationNumber.Should().Be("Unknown");

            PersonDto secondPersonDto = resultDto.SecondPerson;
            secondPersonDto.Should().NotBeNull();
            secondPersonDto.Id.Should().Be(anotherPersonId);
            secondPersonDto.FirstName.Should().Be(createAnotherPersonRequest.FirstName);
            secondPersonDto.LastName.Should().Be(createAnotherPersonRequest.LastNAme);
            secondPersonDto.DateOfBirth.Should().Be("Unknown");
            secondPersonDto.IdentificationNumber.Should().Be("Unknown");
        }

        [Fact]
        public async Task ShouldBeAbleToMatchPeopleThatDoNotExist()
        {
            Guid aPersonId = await testSubject.CreatePerson(createPersonRequest);

            MatchRequest request = new MatchRequest()
            {
                FirstPersonId = aPersonId,
                SecondPersonId = Guid.NewGuid()
            };

            MatchResultDto resultDto = await testSubject.MatchPeople(request);
            resultDto.Should().BeNull();

            request.FirstPersonId = Guid.NewGuid();

            resultDto = await testSubject.MatchPeople(request);
            resultDto.Should().BeNull();

            request.SecondPersonId = aPersonId;

            resultDto = await testSubject.MatchPeople(request);
            resultDto.Should().BeNull();
        }
    }
}
