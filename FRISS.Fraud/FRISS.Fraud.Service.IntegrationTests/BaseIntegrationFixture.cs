﻿using FRISS.Fraud.Api;
using FRISS.Fraud.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using System;

namespace FRISS.Fraud.Service.IntegrationTests
{
    public class BaseIntegrationFixture : IDisposable
    {
        private IServiceCollection ServiceCollection { get; set; }
        public IServiceProvider ServiceProvider { get; private set; }
        private MongoConfiguration DbSettings { get; }

        public BaseIntegrationFixture()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.IntegrationTests.json")
                .Build();

            var connectionString = config.GetConnectionString("db");
            var dbName = $"test_db_{Guid.NewGuid()}";

            DbSettings = new MongoConfiguration(connectionString, dbName);
            ServiceCollection = new ServiceCollection();

            DependencyMapper.MapDependencies(this.ServiceCollection, DbSettings);

            ServiceProvider = this.ServiceCollection.BuildServiceProvider();
        }

        #region IDisposable
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    MongoClient client = new MongoClient(this.DbSettings.ConnectionString);
                    client.DropDatabase(this.DbSettings.DatabaseName);
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~BaseIntegrationFixture()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
