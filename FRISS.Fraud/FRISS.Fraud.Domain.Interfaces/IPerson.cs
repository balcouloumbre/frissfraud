﻿using System;

namespace FRISS.Fraud.Domain.Interfaces
{
    public interface IPerson : IIdentity
    {
        DateTime? DateOfBirth { get; }
        string FirstName { get; }
        int? IdentificationNumber { get; }
        string LastName { get; }

        void SetDateOfBirth(DateTime dateOfBirth);
        void SetIdentificationNumber(int identificationNumber);
    }
}
