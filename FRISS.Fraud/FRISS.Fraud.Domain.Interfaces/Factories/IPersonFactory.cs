﻿namespace FRISS.Fraud.Domain.Interfaces.Factories
{
    public interface IPersonFactory
    {
        IPerson CreatePerson(string firstName, string lastName);
    }
}
