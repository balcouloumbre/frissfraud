﻿namespace FRISS.Fraud.Domain.Interfaces.Factories
{
    public interface IPersonMatcherFactory
    {
        IPersonMatcher CreatePersonMatcher();
    }
}
