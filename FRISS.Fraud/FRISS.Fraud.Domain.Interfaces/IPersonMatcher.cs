﻿namespace FRISS.Fraud.Domain.Interfaces
{
    public interface IPersonMatcher
    {
        double Match(IPerson aPerson, IPerson anotherPerson);
    }
}
