﻿using System;

namespace FRISS.Fraud.Domain.Interfaces
{
    public interface IIdentity
    {
        Guid Id { get; }
    }
}
