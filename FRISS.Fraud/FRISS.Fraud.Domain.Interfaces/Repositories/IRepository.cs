﻿using System;
using System.Threading.Tasks;

namespace FRISS.Fraud.Domain.Interfaces.Repositories
{
    public interface IRepository<T>
    where T : IIdentity
    {
        Task DeleteAll();
        Task<T> GetById(Guid id);
        Task Add(T entity);
        Task Update(T entity);
    }
}
