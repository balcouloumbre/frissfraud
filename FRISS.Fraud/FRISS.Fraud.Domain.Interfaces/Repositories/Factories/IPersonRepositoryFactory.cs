﻿namespace FRISS.Fraud.Domain.Interfaces.Repositories.Factories
{
    public interface IPersonRepositoryFactory
    {
        IPersonRepository CreateRepository();
    }
}
