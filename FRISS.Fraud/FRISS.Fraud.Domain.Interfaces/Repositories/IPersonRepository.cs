﻿namespace FRISS.Fraud.Domain.Interfaces.Repositories
{
    public interface IPersonRepository : IRepository<IPerson>
    {
    }
}
