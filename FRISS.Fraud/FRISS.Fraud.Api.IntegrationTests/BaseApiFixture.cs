﻿using FRISS.Fraud.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MongoDB.Driver;
using System;
using System.IO;

namespace FRISS.Fraud.Api.IntegrationTests
{
    public class BaseApiFixture : IDisposable
    {
        public IHost Host { get; private set; }

        private IServiceCollection ServiceCollection { get; set; }
        public IServiceProvider ServiceProvider { get; private set; }
        private IConfiguration Configuration { get; set; }

        private MongoConfiguration DbSettings { get; set; }

        public BaseApiFixture()
        {
            Configuration = CreateConfiguration();

            IConfigurationSection dbConnectionSettings = Configuration.GetSection("ConnectionStrings:dbHost");
            string connectionString = dbConnectionSettings.Value;

            IConfigurationSection dbNameSettings = Configuration.GetSection("ConnectionStrings:db");
            string dbName = dbNameSettings.Value;
            DbSettings = new MongoConfiguration(connectionString, dbName);

            ServiceCollection = new ServiceCollection();

            DependencyMapper.MapDependencies(ServiceCollection, DbSettings);

            ServiceProvider = this.ServiceCollection.BuildServiceProvider();

            IHostBuilder hostBuilder = new HostBuilder()
                .ConfigureWebHost(webHost =>
                {
                    webHost.UseTestServer();
                    webHost.UseStartup<TestStartup>();
                    webHost.UseConfiguration(Configuration);
                });

            this.Host = hostBuilder.Start();
        }
        private IConfiguration CreateConfiguration()
        {
            IConfiguration configuration = new ConfigurationBuilder()
                                    .SetBasePath(Directory.GetCurrentDirectory())
                                    .AddJsonFile("appsettings.Test.json")
                                    .Build();

            return configuration;
        }

        #region IDisposable
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    MongoClient client = new MongoClient(this.DbSettings.ConnectionString);
                    client.DropDatabase(this.DbSettings.DatabaseName);
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~BaseApiFixture()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
