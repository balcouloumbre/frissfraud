﻿using FluentAssertions;
using FRISS.Fraud.Domain.Interfaces.Repositories;
using FRISS.Fraud.Domain.Interfaces.Repositories.Factories;
using FRISS.Fraud.Service.Models.PersonService;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Xunit;

namespace FRISS.Fraud.Api.IntegrationTests
{
    public class PersonApiTests : IClassFixture<BaseApiFixture>
    {
        private readonly BaseApiFixture _apiFixture;

        private readonly IPersonRepositoryFactory personRepositoryFactory;

        private readonly CreatePersonRequest createPersonRequest;
        private readonly CreatePersonRequest createAnotherPersonRequest;

        public PersonApiTests(BaseApiFixture apiFixture)
        {
            this._apiFixture = apiFixture;

            createPersonRequest = new CreatePersonRequest()
            {
                FirstName = "John",
                LastNAme = "Doe"
            };

            createAnotherPersonRequest = new CreatePersonRequest()
            {
                FirstName = "Jane",
                LastNAme = "Doe"
            };

            personRepositoryFactory = _apiFixture.ServiceProvider.GetService<IPersonRepositoryFactory>();

            CleanupData().GetAwaiter().GetResult();
        }

        private async Task CleanupData()
        {
            IPersonRepository personRepository = personRepositoryFactory.CreateRepository();
            await personRepository.DeleteAll();
        }

        private async Task<HttpResponseMessage> CreatePerson(CreatePersonRequest request)
        {
            HttpClient httpClient = _apiFixture.Host.GetTestClient();
            HttpResponseMessage response = await httpClient.PostAsJsonAsync($"/create", request);

            return response;
        }

        private async Task<HttpResponseMessage> MatchPeople(MatchRequest request)
        {
            HttpClient httpClient = _apiFixture.Host.GetTestClient();
            HttpResponseMessage response = await httpClient.PostAsJsonAsync($"/match", request);

            return response;
        }

        private async Task<HttpResponseMessage> Get(Guid id)
        {
            HttpClient httpClient = _apiFixture.Host.GetTestClient();
            HttpResponseMessage response = await httpClient.GetAsync($"get?id={id}");

            return response;
        }

        [Fact]
        public async Task ShouldBeAbleToGetAnOkStatusFromTheApi()
        {
            HttpClient httpClient = _apiFixture.Host.GetTestClient();

            HttpResponseMessage response = await httpClient.GetAsync($"/status");

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task ShouldBeAbleToCreateAndGetAPerson()
        {
            HttpResponseMessage response = await CreatePerson(createPersonRequest);
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            string stringResponse = await response.Content.ReadAsStringAsync();
            Guid id = JsonConvert.DeserializeObject<Guid>(stringResponse);
            id.Should().NotBeEmpty();

            response = await Get(id);
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            stringResponse = await response.Content.ReadAsStringAsync();
            PersonDto dto = JsonConvert.DeserializeObject<PersonDto>(stringResponse);
            dto.Should().NotBeNull();
            dto.Id.Should().Be(id);
            dto.FirstName.Should().Be(createPersonRequest.FirstName);
            dto.LastName.Should().Be(createPersonRequest.LastNAme);
            dto.DateOfBirth.Should().Be("Unknown");
            dto.IdentificationNumber.Should().Be("Unknown");
        }

        [Fact]
        public async Task ShouldBeAbleToCreateAndGetAPersonWithFullData()
        {
            createPersonRequest.DateOfBirth = DateTime.Now.AddYears(-30);
            createPersonRequest.IdentificationNumber = 123456768;

            HttpResponseMessage response = await CreatePerson(createPersonRequest);
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            string stringResponse = await response.Content.ReadAsStringAsync();
            Guid id = JsonConvert.DeserializeObject<Guid>(stringResponse);
            id.Should().NotBeEmpty();

            response = await Get(id);
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            stringResponse = await response.Content.ReadAsStringAsync();
            PersonDto dto = JsonConvert.DeserializeObject<PersonDto>(stringResponse);
            dto.Should().NotBeNull();
            dto.Id.Should().Be(id);
            dto.FirstName.Should().Be(createPersonRequest.FirstName);
            dto.LastName.Should().Be(createPersonRequest.LastNAme);
            dto.DateOfBirth.Should().Be(createPersonRequest.DateOfBirth.Value.ToString("dd/MM/yyyy"));
            dto.IdentificationNumber.Should().Be(createPersonRequest.IdentificationNumber.ToString());
        }

        [Fact]
        public async Task ShouldNotBeAbleToCreateAPersonWithoutTheCorrectVerb()
        {
            HttpClient httpClient = _apiFixture.Host.GetTestClient();
            HttpResponseMessage response = await httpClient.PutAsJsonAsync($"/create", createPersonRequest);

            response.StatusCode.Should().Be(HttpStatusCode.MethodNotAllowed);
        }

        [Fact]
        public async Task ShouldNotBeAbleToCreateAPersonWithoutTheCorrectPayload()
        {
            Guid notACreateRequest = Guid.NewGuid();

            HttpClient httpClient = _apiFixture.Host.GetTestClient();
            HttpResponseMessage response = await httpClient.PostAsJsonAsync($"/create", notACreateRequest);

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task ShouldNotBeAbleToCreateAPersonWithoutAName()
        {
            createPersonRequest.FirstName = string.Empty;

            HttpClient httpClient = _apiFixture.Host.GetTestClient();
            HttpResponseMessage response = await httpClient.PostAsJsonAsync($"/create", createPersonRequest);

            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }

        [Fact]
        public async Task ShouldBeAbleToGetAPersonThatDoesNotExist()
        {
            HttpResponseMessage response = await Get(Guid.NewGuid());
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task ShouldNotBeAbleToGetAPersonWithoutTheCorrectVerb()
        {
            HttpClient httpClient = _apiFixture.Host.GetTestClient();

            HttpResponseMessage response = await httpClient.PostAsJsonAsync($"/get", Guid.NewGuid());

            response.StatusCode.Should().Be(HttpStatusCode.MethodNotAllowed);
        }

        [Fact]
        public async Task ShouldBeAbleToMatchTwoPeople()
        {
            HttpResponseMessage response = await CreatePerson(createPersonRequest);
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            string stringResponse = await response.Content.ReadAsStringAsync();
            Guid aPersonId = JsonConvert.DeserializeObject<Guid>(stringResponse);

            response = await CreatePerson(createAnotherPersonRequest);
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            stringResponse = await response.Content.ReadAsStringAsync();
            Guid anotherPersonId = JsonConvert.DeserializeObject<Guid>(stringResponse);

            MatchRequest matchRequest = new MatchRequest()
            {
                FirstPersonId = aPersonId,
                SecondPersonId = anotherPersonId
            };

            response = await MatchPeople(matchRequest);
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            stringResponse = await response.Content.ReadAsStringAsync();
            MatchResultDto matchResult = JsonConvert.DeserializeObject<MatchResultDto>(stringResponse);
            matchResult.Should().NotBeNull();
            matchResult.Match.Should().Be($"40.00%");

            PersonDto firstPersonDto = matchResult.FirstPerson;
            firstPersonDto.Should().NotBeNull();
            firstPersonDto.Id.Should().Be(aPersonId);
            firstPersonDto.FirstName.Should().Be(createPersonRequest.FirstName);
            firstPersonDto.LastName.Should().Be(createPersonRequest.LastNAme);
            firstPersonDto.DateOfBirth.Should().Be("Unknown");
            firstPersonDto.IdentificationNumber.Should().Be("Unknown");

            PersonDto secondPersonDto = matchResult.SecondPerson;
            secondPersonDto.Should().NotBeNull();
            secondPersonDto.Id.Should().Be(anotherPersonId);
            secondPersonDto.FirstName.Should().Be(createAnotherPersonRequest.FirstName);
            secondPersonDto.LastName.Should().Be(createAnotherPersonRequest.LastNAme);
            secondPersonDto.DateOfBirth.Should().Be("Unknown");
            secondPersonDto.IdentificationNumber.Should().Be("Unknown");
        }

        [Fact]
        public async Task ShouldBeAbleToMatchTwoPeopleThatDontExist()
        {
            MatchRequest matchRequest = new MatchRequest()
            {
                FirstPersonId = Guid.NewGuid(),
                SecondPersonId = Guid.NewGuid()
            };

            HttpResponseMessage response = await MatchPeople(matchRequest);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task ShouldNotBeAbleToMatchTwoWithoutTheCorrectVerb()
        {
            MatchRequest matchRequest = new MatchRequest()
            {
                FirstPersonId = Guid.NewGuid(),
                SecondPersonId = Guid.NewGuid()
            };

            HttpClient httpClient = _apiFixture.Host.GetTestClient();
            HttpResponseMessage response = await httpClient.PutAsJsonAsync($"/match", matchRequest);
            response.StatusCode.Should().Be(HttpStatusCode.MethodNotAllowed);
        }

        [Fact]
        public async Task ShouldNotBeAbleToMatchTwoWithoutTheCorrectPayload()
        {
            Guid notAMatchRequest = Guid.NewGuid();

            HttpClient httpClient = _apiFixture.Host.GetTestClient();
            HttpResponseMessage response = await httpClient.PostAsJsonAsync($"/match", notAMatchRequest);
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
    }
}
