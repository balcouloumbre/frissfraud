﻿using FRISS.Fraud.Api.Controllers;
using FRISS.Fraud.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FRISS.Fraud.Api.IntegrationTests
{
    public class TestStartup
    {
        public TestStartup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            IConfigurationSection dbConnectionSettings = Configuration.GetSection("ConnectionStrings:dbHost");
            string connectionString = dbConnectionSettings.Value;

            IConfigurationSection dbNameSettings = Configuration.GetSection("ConnectionStrings:db");
            string dbName = dbNameSettings.Value;
            MongoConfiguration mongoConfiguration = new MongoConfiguration(connectionString, dbName);

            DependencyMapper.MapDependencies(services, mongoConfiguration);

            services.AddLogging();

            services.AddRazorPages();

            services.AddMvcCore();
            services.AddMvcCore().AddApplicationPart(typeof(PersonController).Assembly);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();

            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();

            app.UseHttpsRedirection();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
